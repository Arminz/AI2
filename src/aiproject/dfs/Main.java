package aiproject.dfs;

/**
 * Created by armin on 5/1/17.
 */
public class Main {
    public static void main(String[] args) {
        Solver solver = new Solver(IOHandler.readFile());

        solver.solve();

    }

}
