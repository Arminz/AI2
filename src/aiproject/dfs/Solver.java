package aiproject.dfs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by armin on 5/1/17.
 */
public class Solver {
    int n;
    int[][] matrix;
    public Solver(int[][] matrix)
    {
        this.matrix = matrix;
        n = matrix.length;
    }
    public  void solve()
    {
        dfs(0,0);

    }
    public boolean dfs(int row , int column)
    {

        if (row == n && column == 0)
        {
            hooray();
            IOHandler.printFile(matrix);
            IOHandler.print(matrix);
            return true;
        }
        if (matrix[row][column] != 0)
        {
            if (column != n-1 ) {
                if (dfs(row, column + 1))
                    return true;
            }
            else if (column == n-1) {
                if (dfs(row + 1, 0))
                        return true;
            }
            return false;
        }

//        for (int i = 0 ; i <= row ; i ++) {
//            for (int j = 0; j < n; j++)
//                System.out.print(matrix[i][j] + " ");
//            System.out.println();
//        }
//        System.out.println("_________________________________");
//        Scanner scanner = new Scanner(System.in);
//        scanner.nextInt();

        if (row == n && column == 0) {
            hooray();
            IOHandler.printFile(matrix);
            IOHandler.print(matrix);
            return true;
        }
        ArrayList<Integer> possibleChoices = findPossibleChoices(row,column);

//        SSy

        for (int i = 0 ; i < possibleChoices.size(); i ++)
        {


            matrix[row][column] = possibleChoices.get(i);
            if (column != n-1) {
                if (dfs(row, column + 1))
                    return true;
            }
            else if (column == n-1) {
                if (dfs(row + 1, 0))
                    return true;
            }

            matrix[row][column] = 0;
        }
//        System.out.println("Failed");
        if (row == 0 & column == 0)
        {
            notHooray();
        }
        return false;
    }

    private void notHooray() {
        System.out.println("No solution has been found.");

    }

    private void hooray() {
        System.out.println("hooray");
    }

    public ArrayList<Integer> findPossibleChoices(int row , int column){
        boolean isPossibleToBeUsed[] = new boolean[n + 1];
        Arrays.fill(isPossibleToBeUsed,true);


        for (int i = 0 ; i < n ; i ++) {
            isPossibleToBeUsed[matrix[row][i]] = false;
            isPossibleToBeUsed[matrix[i][column]] = false;
        }

        int bigN = (int) Math.sqrt(n);
        int bigRow = (row/bigN) * bigN;
        int bigColumn = (column/bigN) * bigN;
        for (int i = 0 ; i < bigN ; i ++)
            for (int j = 0 ; j < bigN ; j ++)
                isPossibleToBeUsed[matrix[bigRow + i][bigColumn + j]] = false;

        ArrayList<Integer> possibleNumbers = new ArrayList<>();
        for (int i = 1 ; i <= n ; i ++)
            if (isPossibleToBeUsed[i])
                possibleNumbers.add(i);
        return possibleNumbers;
    }
}
