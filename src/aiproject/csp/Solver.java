package aiproject.csp;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by armin on 5/1/17.
 */
public class Solver {
    int n;
    int[][] matrix;
    boolean [][][] possibleChoices;
    int[][] numOfPossibleChoices;

    public Solver(int[][] matrix)
    {
        this.matrix = matrix;
        n = matrix.length;

        possibleChoices = new boolean[n][n][n+1];
        numOfPossibleChoices = new int[n][n];

        for (int i = 0 ; i < n ; i ++)
            for (int j = 0 ; j < n ; j ++) {
                for (int k = 1; k <= n; k++)
                    possibleChoices[i][j][k] = true;
                numOfPossibleChoices[i][j] = n;
            }

    }

    public void solve()
    {
        for (int i = 0 ; i < n ; i ++)
            for(int j = 0 ; j < n ; j ++)
                updateConsistency(i,j);
        if (recursiveSolution()) {
            IOHandler.printFile(matrix);
            IOHandler.print(matrix);
            hooray();
        }

        done();



    }

    private void done() {
        System.out.print("done");
    }
    private int countFull()
    {
        int count = 0;
        for (int i = 0 ; i < n ; i ++)
            for (int j = 0 ; j < n ; j ++)
                if (matrix[i][j] != 0)
                    count ++;
        return count;
    }

    private boolean recursiveSolution() {

//        IOHandler.print(matrix);
//        System.out.println("--------------------------");
//        IOHandler.print(possibleChoices);
//        System.out.println("--------------------------");
//        IOHandler.print(numOfPossibleChoices);
        Cell bestCell = findBestCell();
        if (bestCell == null)
            return true;

//        System.out.println("--------------------------");
//        System.out.println(bestCell.getX() + " : " + bestCell.getY());


//        System.out.println("countFull : " + countFull());

        for (int i = 1 ; i <= n ; i ++)
            if (possibleChoices[bestCell.getX()][bestCell.getY()][i])
            {
                forwardChecking(bestCell.getX(),bestCell.getY(),i);
                if (recursiveSolution())
                    return true;
//                backward(bestCell.getX(),bestCell.getY());
                matrix[bestCell.getX()][bestCell.getY()] = 0;
//                updateConsistency(bestCell.getX(),bestCell.getY());
                for (int ii = 0 ; ii < n ; ii ++)
                    for (int jj = 0 ; jj < n ; jj ++) {
                        for (int kk = 1; kk <= n; kk++)
                            possibleChoices[ii][jj][kk] = true;
                        numOfPossibleChoices[ii][jj] = n;
                    }
                for (int ii = 0 ; ii < n ; ii ++)
                    for(int jj = 0 ; jj < n ; jj ++)
                        updateConsistency(ii,jj);
            }
        return false;

    }

    private void backward(int row, int column) {
        if (matrix[row][column] == 0)
            throw new RuntimeException("مقداربرداری از خانه ای که مقدار نداره");
//        System.out.println(row + " : " + column + " < - ");
        int x = matrix[row][column];
        matrix[row][column] = 0;
        for (int i = 0; i < n ; i ++) {
            possibleChoices[row][i][x] = true;
            numOfPossibleChoices[row][i] ++;
            possibleChoices[i][column][x] = true;
            numOfPossibleChoices[i][column] ++;

        }

        int bigN = (int) Math.sqrt(n);
        int bigRow = (row / bigN) * bigN;
        int bigColumn = (column / bigN) * bigN;
        for (int i = 0; i < bigN; i++)
            for (int j = 0; j < bigN; j++) {
                possibleChoices[bigRow + i][bigColumn + j][x] = true;
                numOfPossibleChoices[bigRow + i][bigColumn + j] ++;
            }

    }

    private void hooray() {
        System.out.println("hooray!!!");
    }

    private Cell findBestCell(){
        Cell bestCell = null;
        for  (int i = 0 ; i < n ; i ++)
            for (int j = 0 ; j < n ; j ++)
                if (matrix[i][j] == 0 && (bestCell == null || numOfPossibleChoices[i][j] < bestCell.getNumOfPossibleChoices()))
                    bestCell = new Cell(i,j);
//        if (bestCell != null)
//        System.out.println("D" + bestCell.getNumOfPossibleChoices());
        return bestCell;
    }
    private void forwardChecking(int row, int column,int x)
    {
        if (matrix[row][column] != 0)
            throw new RuntimeException("مقدار دهی دوباره به خانه ای که مقدار داره");
//        System.out.println(row + " : " + column + " - > " + x);
        matrix[row][column] = x;
        for (int i = 0; i < n ; i ++) {
            if (possibleChoices[row][i][x]) {
                possibleChoices[row][i][x] = false;
                numOfPossibleChoices[row][i] --;
            }
            if (possibleChoices[i][column][x]) {
                possibleChoices[i][column][x] = false;
                numOfPossibleChoices[i][column] --;
            }
        }

        int bigN = (int) Math.sqrt(n);
        int bigRow = (row / bigN) * bigN;
        int bigColumn = (column / bigN) * bigN;
        for (int i = 0; i < bigN; i++)
            for (int j = 0; j < bigN; j++) {
                if (possibleChoices[bigRow + i][bigColumn + j][x]) {
                    possibleChoices[bigRow + i][bigColumn + j][x] = false;
                    numOfPossibleChoices[bigRow + i][bigColumn + j] --;
                }
            }

    }

    private void updateConsistency(int row,int column) {

        for (int i = 0; i < n; i++) {
            if (possibleChoices[row][column][matrix[row][i]]) {
                possibleChoices[row][column][matrix[row][i]] = false;
                numOfPossibleChoices[row][column]--;
            }
            if (possibleChoices[row][column][matrix[i][column]]) {
                possibleChoices[row][column][matrix[i][column]] = false;
                numOfPossibleChoices[row][column]--;
            }
        }

        int bigN = (int) Math.sqrt(n);
        int bigRow = (row / bigN) * bigN;
        int bigColumn = (column / bigN) * bigN;
        for (int i = 0; i < bigN; i++)
            for (int j = 0; j < bigN; j++) {
                if (possibleChoices[row][column][matrix[i][column]]) {
                    possibleChoices[row][column][matrix[bigRow + i][bigColumn + j]] = false;
                    if (possibleChoices[row][column][matrix[i][column]])
                        numOfPossibleChoices[row][column]--;
                }
            }

    }
    private class Cell{
        public Cell(int x, int y) {
            this.x = x;
            this.y = y;
        }

        private int x;
        private int y;

        public int getNumOfPossibleChoices() {
            return numOfPossibleChoices[x][y];
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }

}
