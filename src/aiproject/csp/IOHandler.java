package aiproject.csp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

/**
 * Created by armin on 5/1/17.
 */
public class IOHandler {
    public static int[][] readFile()  {
//        FileInputStream fileInputStream = new FileInputStream("matrix.txt.txt");
//
//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
        Scanner scanner = null;
        boolean duplicate = false;
        try {
            scanner = new Scanner(new File("matrix4v4A.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }


        int n = scanner.nextInt();
        int matrix[][] = new int[n][n];
        for (int i = 0 ; i < n ; i ++)
            for (int j = 0 ; j < n ; j ++) {
                matrix[i][j] = scanner.nextInt();
//                System.out.println(matrix[i][j]);
            }
        return matrix;
    }
    public static void printFile(int[][] matrix){
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("csp_matrix.txt","UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        int n = matrix.length;

        for (int i = 0 ; i < n ; i ++) {
            for (int j = 0; j < n; j++)
                writer.print(matrix[i][j] + " ");
            writer.println();
        }
        writer.close();

    }
    public static void print(int[][] matrix)
    {
        int n = matrix.length;
        for (int i = 0 ; i < n ; i ++) {
            for (int j = 0; j < n; j++)
                System.out.print(matrix[i][j] + " ");
            System.out.println();
        }


    }

    public static void print(boolean[][][] matrix) {
        int n = matrix.length;
        for (int i = 0 ; i < n ; i ++) {
            for (int j = 0; j < n; j++)
            {
                System.out.print("{ ");
                for (int k = 0 ; k < n ; k ++)
                    if (matrix[i][j][k])
                    System.out.print(k + " ");
                System.out.print("}");
            }
            System.out.println();
        }

    }
}
